var ImageEditorActivator = {
	initialize: function() {
		this.onOpen = ImageEditorActivator.onOpen.bind(this);		
	},
	
	onOpen: function() {
		var windowWidth = Element.getDimensions(window.top.document.body).width;
        var windowHeight = Element.getDimensions(window.top.document.body).height;
		iframe = window.top.document.getElementById('imageEditorIframe');
		if(iframe != null) {
			iframe.parentNode.removeChild(iframe);
		}
		iframe = window.top.document.createElement('iframe');
		fileToEdit = $('ImageEditorActivator').firstChild.src;
		iframe.setAttribute("src","admin/ImageEditor?fileToEdit=" + fileToEdit);
		iframe.id = 'imageEditorIframe';
		iframe.style.width = windowWidth - 30 + 'px';
		iframe.style.height = windowHeight + 10 + 'px';
		iframe.style.zIndex = "1000";
		iframe.style.position = "absolute";
		iframe.style.top = "-2%";
		iframe.style.left = "1.5%";
		window.top.document.body.appendChild(iframe);
		divLeft = window.top.document.createElement('div');
		divRight = window.top.document.createElement('div');
        divLeft.style.width = "1.5%";
        divLeft.style.height = "300%";
        divLeft.style.zIndex = "1000";
        divLeft.style.top = "-1.5%";
        divLeft.style.position = "absolute";
        divRight.style.width = "1.5%";
        divRight.style.height = "300%";
        divRight.style.zIndex = "1000";
        divRight.style.top = "-1.5%";
        divRight.style.position = "absolute";
        divRight.style.left = "98.5%";
		window.top.document.body.appendChild(divLeft);
		window.top.document.body.appendChild(divRight);
	}
		
}