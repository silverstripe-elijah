<?php

global $lang;

$lang['en_US']['CMSMain']['CREATE'] = array(
	'Create a ',
	PR_MEDIUM,
	'"Create a " message, followed by an action (e.g. "contact form")'
);
$lang['en_US']['CMSMain']['REMOVEDFD'] = 'Removed from the draft site';
$lang['en_US']['CMSMain']['RESTORE'] = 'Restore';
$lang['en_US']['CMSMain']['DELETEFP'] = 'Delete from the published site';
$lang['en_US']['CMSMain']['SAVE'] = 'Save';
$lang['en_US']['CMSMain']['PAGENOTEXISTS'] = 'This page doesn\'t exist';
$lang['en_US']['CMSMain']['NEW'] = array(
	'New ',
	PR_MEDIUM,
	'"New " followed by a className'
);
$lang['en_US']['CMSMain']['RESTORED'] = array(
	'Restored \'%s\' successfully',
	PR_MEDIUM,
	'Param %s is a title'
);
$lang['en_US']['CMSMain']['SENTTO'] = array(
	'Sent to %s %s for approval.',
	PR_LOW,
	'First param is first name, and second is surname'
);
$lang['en_US']['CMSMain']['WORKTODO'] = 'You have work to do on these <b>%d</b> pages.';
$lang['en_US']['CMSMain']['NOTHINGASSIGNED'] = 'You have nothing assigned to you.';
$lang['en_US']['CMSMain']['WAITINGON'] = 'You are waiting on other people to work on these <b>%d</b> pages.';
$lang['en_US']['CMSMain']['NOWAITINGON'] = 'You aren\'t waiting on anybody.';
$lang['en_US']['CMSMain']['CHOOSEREPORT'] = '(Choose a report)';
$lang['en_US']['CMSMain']['VERSIONSNOPAGE'] = array(
	'Can\'t find page #%d',
	PR_LOW
);
$lang['en_US']['CMSMain']['ROLLEDBACKVERSION'] = 'Rolled back to version #%d.  New version number is #%d';
$lang['en_US']['CMSMain']['ROLLEDBACKPUB'] = 'Rolled back to published version. New version number is #%d';
$lang['en_US']['CMSMain']['REMOVEDPAGE'] = 'Removed \'%s\' from the published site';
$lang['en_US']['CMSMain']['VIEWING'] = 'You are viewing version #%d, created %s';
$lang['en_US']['CMSMain']['EMAIL'] = 'Email';
$lang['en_US']['CMSMain']['PRINT'] = 'Print';
$lang['en_US']['CMSMain']['ROLLBACK'] = 'Roll back to this version';
$lang['en_US']['CMSMain']['COMPARINGV'] = 'You are comparing versions #%d and #%d';
$lang['en_US']['CMSMain']['COPYPUBTOSTAGE'] = 'Do you really want to copy the published content to the stage site?';
$lang['en_US']['CMSMain']['OK'] = 'OK';
$lang['en_US']['CMSMain']['CANCEL'] = 'Cancel';
$lang['en_US']['CMSMain']['PAGEDEL'] = '%d page deleted ';
$lang['en_US']['CMSMain']['PAGESDEL'] = '%d pages deleted ';
$lang['en_US']['CMSMain']['NOWBROKEN'] = '  The following pages now have broken links:';
$lang['en_US']['CMSMain']['NOWBROKEN2'] = 'Their owners have been emailed and they will fix up those pages.';
$lang['en_US']['CMSMain']['GO'] = 'Go';
$lang['en_US']['CMSMain']['NOCONTENT'] = 'no content';
$lang['en_US']['CMSMain']['TOTALPAGES'] = 'Total pages: ';
$lang['en_US']['CMSMain']['PUBPAGES'] = 'Done: Published %d pages';
$lang['en_US']['CMSMain']['PUBALLFUN'] = '"Publish All" functionality';
$lang['en_US']['CMSMain']['PUBALLFUN2'] = 'Pressing this button will do the equivalent of going to every page and pressing "publish".  It\'s
				intended to be used after there have been massive edits of the content, such as when the site was
				first built.';
$lang['en_US']['CMSMain']['PUBALLCONFIRM'] = array(
	'Please publish every page in the site, copying content stage to live',
	PR_LOW,
	'Confirmation button'
);
$lang['en_US']['CMSMain']['VISITRESTORE'] = array(
	'visit restorepage/(ID)',
	PR_LOW,
	'restorepage/(ID) should not be translated (is an URL)'
);
$lang['en_US']['LeftAndMain']['PERMDEFAULT'] = 'Please choose an authentication method and enter your credentials to access the CMS.';
$lang['en_US']['LeftAndMain']['PERMALREADY'] = 'I\'m sorry, but you can\'t access that part of the CMS.  If you want to log in as someone else, do so below';
$lang['en_US']['LeftAndMain']['PERMAGAIN'] = 'You have been logged out of the CMS.  If you would like to log in again, enter a username and password below.';
$lang['en_US']['LeftAndMain']['HELLO'] = array(
	'Site Content',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['FILESIMAGES'] = array(
	'Files & Images',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['NEWSLETTERS'] = array(
	'Newsletters',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['REPORTS'] = array(
	'Reports',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['SECURITY'] = array(
	'Security',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['STATISTICS'] = array(
	'Statistics',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['HELP'] = array(
	'Help',
	PR_HIGH,
	'Menu title'
);
$lang['en_US']['LeftAndMain']['PAGETYPE'] = 'Page type: ';
$lang['en_US']['LeftAndMain']['SITECONTENT'] = array(
	'Site Content',
	PR_HIGH,
	'Root node on left'
);
$lang['en_US']['LeftAndMain']['SAVEDUP'] = 'Saved';
$lang['en_US']['LeftAndMain']['CHANGEDURL'] = '  Changed URL to \'%s\'';
$lang['en_US']['LeftAndMain']['STATUSTO'] = '  Status changed to \'%s\'';
$lang['en_US']['LeftAndMain']['SAVED'] = 'saved';
$lang['en_US']['LeftAndMain']['PLEASESAVE'] = 'Please Save Page: This page could not be upated because it hasn\'t been saved yet.';
$lang['en_US']['LeftAndMain']['REQUESTERROR'] = 'Error in request';
$lang['en_US']['CMSMain_left.ss']['OPENBOX'] = 'click to open this box';
$lang['en_US']['CMSMain_left.ss']['CLOSEBOX'] = 'click to close box';
$lang['en_US']['CMSMain_left.ss']['SITECONTENT TITLE'] = array(
	'Site Content and Structure',
	PR_HIGH
);
$lang['en_US']['CMSMain_left.ss']['CREATE'] = array(
	'Create...',
	PR_HIGH
);
$lang['en_US']['CMSMain_left.ss']['DELETE'] = array(
	'Delete...',
	PR_HIGH
);
$lang['en_US']['CMSMain_left.ss']['REORDER'] = array(
	'Reorder...',
	PR_HIGH
);
$lang['en_US']['CMSMain_left.ss']['SELECTPAGESDEL'] = 'Select the pages that you want to delete and then click the button below';
$lang['en_US']['CMSMain_left.ss']['DELETECONFIRM'] = 'Delete the selected pages';
$lang['en_US']['CMSMain_left.ss']['DRAGPAGES'] = 'To reorganise your site, drag the pages around as desired.';
$lang['en_US']['CMSMain_left.ss']['SELECTPAGESDUP'] = 'Select the pages that you want to duplicate, whether it\'s children should be included, and where you want the duplicates placed';
$lang['en_US']['CMSMain_left.ss']['KEY'] = 'Key:';
$lang['en_US']['CMSMain_left.ss']['ADDEDNOTPUB'] = 'Added to the draft site and not published yet';
$lang['en_US']['CMSMain_left.ss']['NEW'] = 'new';
$lang['en_US']['CMSMain_left.ss']['DELETEDSTILLLIVE'] = 'Deleted from the draft site but still on the live site';
$lang['en_US']['CMSMain_left.ss']['DEL'] = 'deleted';
$lang['en_US']['CMSMain_left.ss']['EDITEDNOTPUB'] = 'Edited on the draft site and not published yet';
$lang['en_US']['CMSMain_left.ss']['CHANGED'] = 'changed';
$lang['en_US']['CMSMain_left.ss']['TASKLIST'] = 'Tasklist';
$lang['en_US']['CMSMain_left.ss']['WAITINGON'] = 'Waiting on';
$lang['en_US']['CMSMain_left.ss']['PAGEVERSIONH'] = 'Page Version History';
$lang['en_US']['CMSMain_left.ss']['COMPAREMODE'] = 'Compare mode (click 2 below)';
$lang['en_US']['CMSMain_left.ss']['SHOWUNPUB'] = 'Show unpublished versions';
$lang['en_US']['CMSMain_left.ss']['COMMENTS'] = 'Comments';
$lang['en_US']['CMSMain_left.ss']['SITEREPORTS'] = 'Site Reports';
$lang['en_US']['CMSMain_left.ss']['GO'] = 'Go';
$lang['en_US']['CMSMain_right.ss']['SENDTO'] = 'Send to';
$lang['en_US']['CMSMain_right.ss']['LOADING'] = 'loading...';
$lang['en_US']['CMSMain_right.ss']['STATUS'] = 'Status';
$lang['en_US']['CMSMain_right.ss']['ANYMESSAGE'] = 'Do you have any message for your editor?';
$lang['en_US']['CMSMain_right.ss']['MESSAGE'] = 'Message';
$lang['en_US']['CMSMain_right.ss']['SUBMIT'] = 'Submit for approval';
$lang['en_US']['CMSMain_right.ss']['WELCOMETO'] = 'Welcome to';
$lang['en_US']['CMSMain_right.ss']['CHOOSEPAGE'] = 'Please choose a page from the left.';
$lang['en_US']['CMSRight.ss']['WELCOMETO'] = 'Welcome to';
$lang['en_US']['CMSRight.ss']['CHOOSEPAGE'] = 'Please choose a page from the left.';
$lang['en_US']['LeftAndMain.ss']['LOADING'] = array(
	'Loading...',
	PR_HIGH
);
$lang['en_US']['LeftAndMain.ss']['SSWEB'] = 'Silverstripe Website';
$lang['en_US']['LeftAndMain.ss']['APPVERSIONTEXT1'] = 'This is the';
$lang['en_US']['LeftAndMain.ss']['APPVERSIONTEXT2'] = 'version that you are currently running, technically it\'s the CVS branch';
$lang['en_US']['LeftAndMain.ss']['LOGGEDINAS'] = 'Logged in as';
$lang['en_US']['LeftAndMain.ss']['LOGOUT'] = 'log out';
$lang['en_US']['LeftAndMain.ss']['VIEWPAGEIN'] = 'Page view:';
$lang['en_US']['LeftAndMain.ss']['SWITCHTO'] = 'Switch to:';
$lang['en_US']['LeftAndMain.ss']['EDIT'] = 'Edit';
$lang['en_US']['LeftAndMain.ss']['DRAFTS'] = 'Draft Site';
$lang['en_US']['LeftAndMain.ss']['PUBLIS'] = 'Published Site';
$lang['en_US']['LeftAndMain.ss']['ARCHS'] = 'Archived Site';

?>