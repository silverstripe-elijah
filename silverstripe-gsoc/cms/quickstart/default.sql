INSERT INTO SiteTree SET 
ClassName = 'Page', Title = 'About Us', URLSegment = 'about-us',
Content = '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>',
Created = NOW(), LastEdited = NOW(), Status = 'Published', ShowInMenus = 1, ShowInSearch = 1, Sort = 2;

INSERT INTO SiteTree SET 
ClassName = 'Page', Title  = 'Contact Us', URLSegment = 'contact-us',
Content = '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>',
Created = NOW(), LastEdited = NOW(), Status = 'Published', ShowInMenus = 1, ShowInSearch = 1, Sort = 3;

INSERT INTO SiteTree_Live SET 
ClassName = 'Page', Title = 'About Us', URLSegment = 'about-us',
Content = '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>',
Created = NOW(), LastEdited = NOW(), Status = 'Published', ShowInMenus = 1, ShowInSearch = 1, Sort = 2;

INSERT INTO SiteTree_Live SET 
ClassName = 'Page', Title  = 'Contact Us', URLSegment = 'contact-us',
Content = '<p>You can fill this page out with your own content, or delete it and create your own pages.<br /></p>',
Created = NOW(), LastEdited = NOW(), Status = 'Published', ShowInMenus = 1, ShowInSearch = 1, Sort = 3;